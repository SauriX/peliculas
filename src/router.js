import Vue from "vue";
import Router from "vue-router";
//import detalle from "@/views/detalle";
import usuario from "@/components/usuario.vue";
import formulario from "@/components/formulario.vue"

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
        path: "/",
        name: "home",
        component: formulario
        
      },
     
 
    {
      path: "/usuario/:nombre&:email&pass",
      name: "usuario",
      component: usuario
      
    } 
   
  ]
});

